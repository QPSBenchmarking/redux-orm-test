# Redux-Orm-Test

Example of the issue created when attempting to initailise Redux-ORM models with relations for testing purposes. Models are located in "source/models.jsx" and the testing code is located in the "test/test.jsx".

To begin the testing process, create a virtual environment using virtualenv, install node package manager and use the json-package to install the necessary packages, then please run the command "karma start". The default testing browser is Headless-Chrome, which may need to be installed on your local machine. Alternatively you can change this setting in the karma.conf.js file.